#!/usr/bin/ruby

require 'oj'
# require 'pry'
require 'sinatra'
require 'sinatra/reloader' if development?
require 'sinatra/js'
require 'slim'
require 'tilt'
require 'sassc'
require 'token_phrase'

# some server settings

set server: 'thin', connections: []
set :bind, '0.0.0.0'
set :port, '4567' # default 4567
# set :environment, :production

get '/' do
  slim :index
end

put "/life" do

end

get '/life' do
  oshell = params[:oshell]
  talk = params[:talking]
  scolor = params[:scolor]
  lcolor = params[:lcolor]
  dcolor = params[:dcolor]
  tcolor = params[:tcolor]
  iname = params[:iname]
  ichan = params[:ichan]
  ioauth = params[:ioauth]
  iconf = params[:iconf]

  if talk == "on"
    talk = true
  else
    talk = false
  end

  if oshell == "on"
    oshell = true
  else
    oshell = false
  end

  botdat = {
    talk: talk, shell: oshell, shellColor: scolor, lifeColor: lcolor, dmgColor: dcolor,
    textColor: tcolor, inputName: iname, inputChan: ichan, inputOauth: ioauth, inputConf: iconf
  }

  botdat = botdat.to_json

  slim :life
  # binding.pry
end


get '/application.css' do
  scss :application
end

get '/lifebar.css' do
  scss :lifebar
end

get '/demo.css' do
  scss :demo
end

get '/application.js' do
  content_type 'text/javascript'
  send_file File.join('views', 'application.js')
end

get '/lifebit.js' do
  content_type 'text/javascript'
  send_file File.join('views', 'lifebit.js')
end
