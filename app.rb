#!/usr/bin/env ruby
# frozen_string_literal = true

require 'sinatra'
require 'sinatra/reloader' if development?
require 'sinatra/js'
require 'slim'
require 'tilt'
require 'sassc'
require 'httparty'
require 'configatron'
require_relative 'config.rb'

# Server Settings
set server: 'thin', connections: []
set :bind, '0.0.0.0'
set :port, '3000'
# set :enviroment, :production

get '/' do
  slim :index
end

get '/application.css' do
  scss :application
end

get '/application.js' do
  content_type 'text/javascript'
  send_file File.join('views', 'application.js')
end